import math
import subprocess
from numpy.lib.arraypad import pad
from powerup import *
import random
import random
from paddle import paddle
from ball import Ball
import sys
import time
import numpy as np
from colorama import Fore, Back, Style 
import global_var as glob
from global_var import printframe,start_time,balls,laser_balls
from input import Get,input_to
from brick import *

def assign_rainbow(pos):        # decides whether a particular brick is going to be rainbow or not
    val=random.randint(0,100)
    if(val<glob.rainbow_prob):
        glob.rainbow_bricks.append(pos)
        glob.isBrick[pos].has_hit=0

def change_brick(pos):          # shuffles the brick color
    if(glob.isBrick[(pos)]==0):
        glob.rainbow_bricks[:]=[tup for tup in glob.rainbow_bricks if tup!=pos]
        return
    val=random.randint(1,3)
    glob.isBrick[(pos)].level=val

def move_to_level_2():
    # start
    offset=0
    glob.rainbow_bricks.clear()
    for row in range(2,11,1):
        for col in range(2+offset,18-offset):
            val=random.randint(0,2)
            if(val==0):
                glob.isBrick[(row,col)]=Brick1((row,col))
            elif(val==1):
                glob.isBrick[(row,col)]=Brick2((row,col))
            else:
                glob.isBrick[(row,col)]=Brick3((row,col))
            glob.frame[row][col]='|'
            assign_rainbow((row,col))
        offset+=1
    
    unbreakcount=0
    unbreak=[]

    for obj in glob.rainbow_bricks:                      # making sure the rainbows aren't being made unbreakable
        unbreak.append(obj)

    while(unbreakcount<20):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,13)
        if((rand_row,rand_col) not in unbreak):
            unbreakcount+=1
            glob.isBrick[(rand_row,rand_col)]=Brick((row,col))
            unbreak.append((rand_row,rand_col))
            glob.frame[rand_row][rand_col]='|'

    for obj in glob.rainbow_bricks:                      # removing rainbows from unbreakable so that they can get powerups
        unbreak[:] = [tup for tup in unbreak if tup!=obj]
    
    for col in range (7,13):
        glob.isBrick[(4,col)]=ExBrick((4,col))
        glob.isBrick[(5,col)]=ExBrick((5,col))


    powerup_given=[]

    glob.isBrick[(5,7)].give_powerup(LongPaddle((5,7)))
    powerup_given.append((5,7))

    
    i=0
    while(i<1):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(LongPaddle((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<2):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(ShortPaddle((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<3):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(Double_Ball((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<4):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(Thru_Ball((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<5):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(FastBall((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<6):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(PaddleGrab((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<7):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(ShootingPaddle((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    while(i<7):
        rand_col=random.randint(2,18)
        rand_row=random.randint(2,11)
        if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
            i+=1
            glob.isBrick[(rand_row,rand_col)].give_powerup(Fireball((rand_row,rand_col)))
            powerup_given.append((rand_row,rand_col))

    glob.bricks_start=2
    glob.bricks_end=10


def move_to_level_3():
    # glob.frame[2][10]='|'
    # temp=[(2,paddle.start),(2,paddle.start+1),(2,paddle.start+3),(2,paddle.start+4),(1,paddle.start+1),(1,paddle.start+2),(1,paddle.start+3)]
    
    # for pos in temp:
    #     glob.isBrick[pos]=Brick(pos)
    #     glob.isBrick[pos].rain
        # glob.frame[pos[0]][pos[1]]='|'
    unbreakcount=0
    unbreak=[]

    while(unbreakcount<10):
        rand_col=random.randint(2,18)
        rand_row=random.randint(8,14)
        if((rand_row,rand_col) not in unbreak):
            unbreakcount+=1
            glob.isBrick[(rand_row,rand_col)]=Brick((row,col))
            unbreak.append((rand_row,rand_col))

    paddle.move_ufo()
    glob.ufo_start_time=time.time()
    
    


def level_up():

    glob.level_start=time.time()
    glob.falling_soon=0
    
    def reset():
        for obj in balls:
            glob.frame[int(obj.pos[0])][int(obj.pos[1])]=obj.placeholder
        for obj in laser_balls:
            glob.frame[int(obj.pos[0])][int(obj.pos[1])]=obj.placeholder
        for obj in glob.powerups:
            obj.deactivate()
        laser_balls.clear()
        balls.clear()
        paddle.positions.clear()
        glob.rainbow_bricks.clear()
        balls.append(Ball())
        paddle.positions[balls[0].num]=math.floor(paddle.length/2)
        balls[0].hold=1
        paddle.move('d')

        for col in range(2,19):
            for row in range(2,19):
                glob.isBrick[(row,col)]=0
                glob.frame[row][col]='.'

        subprocess.Popen(['aplay', './Sounds/NewLevel.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
        print ("\x1b[2J\x1b[H")
        print("You're Moving Up to Level: "+str(glob.game_level+1))
        print("Press C to continue")
        while(1):
            text=input_to(Get())
            if(text=='c' or text=='C'):
                break

    if(glob.game_level==3):
        glob.endgame()
    if(glob.game_level==2):
        reset()
        move_to_level_3()
    if(glob.game_level==1):
        reset()
        move_to_level_2()



    glob.game_level+=1

balls.append(Ball())
paddle.positions[balls[0].num]=math.floor(paddle.length/2)
balls[0].hold=1
paddle.move('d')
for col in range (2,18):
    for row in range(2,5,1):
        glob.isBrick[(row,col)]=Brick3((row,col))
        glob.frame[row][col]='|'
        assign_rainbow((row,col))
    
    for row in range(5,10,1):
        glob.isBrick[(row,col)]=Brick2((row,col))
        glob.frame[row][col]='|'
        assign_rainbow((row,col))

    for row in range(10,13,1):
        glob.isBrick[(row,col)]=Brick1((row,col))
        glob.frame[row][col]='|'
        assign_rainbow((row,col))

glob.isBrick[(12,3)].give_powerup(Fireball((12,3)))
glob.isBrick[(11,3)].give_powerup(LongPaddle((11,3)))
unbreakcount=0
unbreak=[]

for obj in glob.rainbow_bricks:                      # making sure the rainbows aren't being made unbreakable
    unbreak.append(obj)

while(unbreakcount<24):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak):
        unbreakcount+=1
        glob.isBrick[(rand_row,rand_col)]=Brick((row,col))
        unbreak.append((rand_row,rand_col))

for obj in glob.rainbow_bricks:                      # removing rainbows from unbreakable so that they can get powerups
    unbreak[:] = [tup for tup in unbreak if tup!=obj]


# for col in (12,18):
#     for row in ([4,11]):
#         # print(str(row)+" "+str(col))
#         glob.isBrick[(row,col)]=ExBrick((row,col))

for row in ([6]):
    for col in range(2,8):
        # print(str(row)+" "+str(col))
        glob.isBrick[(row,col)]=ExBrick((row,col))

for row in ([8]):
    for col in range(12,18):
        # print(str(row)+" "+str(col))
        glob.isBrick[(row,col)]=ExBrick((row,col))


powerup_given=[]
i=0
while(i<2):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(LongPaddle((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<4):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(ShortPaddle((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<6):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(Double_Ball((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<8):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(Thru_Ball((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<10):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(FastBall((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<12):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(PaddleGrab((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<14):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(ShootingPaddle((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))

while(i<14):
    rand_col=random.randint(2,18)
    rand_row=random.randint(2,13)
    if(glob.isBrick[(rand_row,rand_col)]!=0 and (rand_row,rand_col) not in unbreak and (rand_row,rand_col) not in powerup_given):
        i+=1
        glob.isBrick[(rand_row,rand_col)].give_powerup(Fireball((rand_row,rand_col)))
        powerup_given.append((rand_row,rand_col))


print("\nPress S to Start the Game\n")
print("Press E mid-game to exit the game\n")
print("Press H to release the ball from hold whenever needed\n")
print("Press L to Level Up mid game\n")
print("Each Powerup lasts for 15 seconds\n")
print("+ : Expand Paddle")
print("- : Shrink Paddle")
print("2 : Ball Multiplier")
print("^ : Ball Speed-Up")
print("! : Thru-Ball")
print("H : Paddle Grab")
print("S : Shooting Paddle")
print("F : FireBall\n")
for obj in glob.color:
    if(obj==4):
        print("\033[1m"+glob.color[obj]+"Color is Unbreakable Brick"+"\033[0m")
    elif (obj>=1 and obj<5):
        print("\033[1m"+glob.color[obj]+"Color is Brick Level "+str(obj)+"\033[0m")
    elif(obj==5):
        print("\033[1m"+glob.color[obj]+"Color is Exploding Brick"+"\033[0m")
    

print(Style.RESET_ALL,end="") 
while(1):
    text=input_to(Get())
    if(text=='s' or text=='S'):
        glob.start_time=time.time()
        glob.level_start=time.time() 
        break
    if(text=='e' or text=='E'):
            sys.exit()
            
frame_time=time.time()
printframe()
activate=0
while(1):
    text=input_to(Get())
    if(text!=None):
        if(text=='e' or text=='E'):
            sys.exit()
        elif(text=='l' or text=='L'):
            level_up()
        elif(text=='d' or text=='a' or text=='D' or text=='A'):
            paddle.move(text)
            printframe()
        elif(text=='h' or text=='H'):
            paddle.positions.clear()
            for ball in balls:
                ball.hold=0
    # elapsed=time.time()-glob.start_time
    # # print(elapsed)
    # if elapsed>4 and activate==0:
    #     # print('IN ELAPSED')
    #     activate=1
    #     temp=Double_Ball((10,10))
    #     temp.activate()
    cur_time=time.time()
    if(glob.game_level==3):
        if(glob.ufo_health<=int(0.75*glob.ufo_max_health) and glob.ufo_defence[0]!=1):
            if(balls[0].pos[0]>6):
                for i in range(1,19):
                    glob.isBrick[(6,i)]=Brick1((6,i))
                glob.ufo_defence[0]=1
        elif(glob.ufo_health<=int(0.50*glob.ufo_max_health) and glob.ufo_defence[1]!=1):
            if(balls[0].pos[0]>7):
                for i in range(1,19):
                    glob.isBrick[(7,i)]=Brick2((7,i))
                glob.ufo_defence[1]=1
        
        if(cur_time-glob.ufo_start_time>2.3):
            temp=Ball()
            temp.pos=(2,paddle.start+2)
            temp.symbol="'"
            temp.x=1
            temp.placeholder='.'
            temp.ufo=1
            laser_balls.append(temp)
            glob.ufo_start_time=cur_time
            subprocess.Popen(['aplay', './Sounds/Laser.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)

    for powerup in glob.powerups:
        if(cur_time-powerup.gravity_time>1):
            powerup.gravity_time=cur_time
            powerup.x+=1
    if(cur_time-frame_time>0.35):
        for powerup in glob.powerups:
            if(powerup.activated==1):
                powerup.check()
            else:
                powerup.move()
        for ball in balls:
            if(ball.hold==0):
                ball.move()
            else:
                # print(paddle.positions)
                ball.pos=(19,paddle.start+paddle.positions[ball.num])
        for ball in laser_balls:
            ball.move()
        for pos in glob.rainbow_bricks:
            change_brick(pos)
        if(paddle.shoot):
            if(cur_time-paddle.shoot_time>1):
                temp=Ball()
                temp.laser=1
                temp.pos=(18,paddle.start)
                temp.symbol="'"
                temp.placeholder=glob.frame[temp.pos[0]][temp.pos[1]]
                laser_balls.append(temp)
                temp=Ball()
                temp.laser=1
                temp.pos=(18,paddle.start-1+paddle.length)
                temp.placeholder=glob.frame[temp.pos[0]][temp.pos[1]]
                temp.symbol="'"
                laser_balls.append(temp)
                paddle.shoot_time=cur_time
                subprocess.Popen(['aplay', './Sounds/Laser.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
        printframe()
    
        frame_time=time.time()
    sys.stdout.flush()
