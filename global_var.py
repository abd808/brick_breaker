import time
import sys
import numpy as np
from colorama import Fore, Back, Style 
import subprocess

file1=open("test.txt","w")

color={0:Fore.WHITE,1:Fore.LIGHTGREEN_EX,2:Fore.MAGENTA,3:Fore.YELLOW,4:Fore.RED,5:Fore.WHITE}

powerup_check={'Fast':0,'Slow':0,'Thru':0,'Double':0,'Increase':0,'Decrease':0}
powerups=[]
balls=[]
laser_balls=[] # holds all the laser balls
rainbow_bricks=[]   # holds all the positions that need to be rainbowed
rainbow_prob=10     # how many bricks will be rainbow
start_time=time.time()
level_start=time.time() # when the current level has started
falling_soon=0 # tells us if the bricks have started falling for this level 
falling_brick_limit=60 # how long after a level starts do the bricks start falling
lives=5
score=0
frame=np.full((20,20), '.')
oldi=0
oldj=0
hold_ball=0
isBrick={}
num=0
power_num=0
game_level=1
ufo_health=10
ufo_max_health=10
ufo_defence=[0,0]
ufo_start_time=''


bricks_start=2
bricks_end=12  # tells us the range of current brick row positions

for i in range (20):
    frame[0][i]='-'
for i in range (1,20):
    frame[i][0]='|'
    frame[i][19]='|'   #definitions

for row in range(20):
    for col in range(20):
        isBrick[(row,col)]=0



# isBrick[(19,19)]=1

# print(isBrick)

# print(frame.shape)
# print(frame[19][19])


def endgame():
    print ("\x1b[2J\x1b[H")
    print("Game Over")
    if(ufo_health==0):
        print("CONGRATS ON DEFEATING THE BIG BAD BOSS")
    print("Your Score "+ str(score))
    subprocess.Popen(['aplay', './Sounds/BossDie.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
    sys.exit()

def printframe():
    global falling_soon
    global falling_brick_limit
    # print(frame)
    print ("\x1b[2J\x1b[H")
    elapsed=time.time()-start_time
    print ("Lives: %d\t\tTime: %02d\t\tScore: %d\t\tLevel: %d" % (lives,elapsed,score,game_level) )
    count=0
    for i in range(0,20):
        for j in range(0,20):
            
            if(isBrick[(i,j)]!=0):
                if(isBrick[(i,j)].level==0):
                    isBrick[(i,j)]=0
                    frame[i][j]='.'
                elif isBrick[(i,j)].level!=4:
                    count+=1
                if(frame[i][j]!='|'):
                    isfine=0
                    for powerup in powerups:
                        if(powerup.pos==(i,j)):
                            isfine=1
                            break
                    if(not isfine):
                        for ball in balls:
                            if ball.pos==(i,j):
                                isfine=1
                                break
                    if(not isfine):
                        frame[i][j]='|'
            if(isBrick[(i,j)]==0 and frame[i][j]=='|' and(i!=19)):
                frame[i][j]='.'
            if((j==0 or j==19) and frame[i][j]=='.'):
                frame[i][j]='|'
            if(frame[i][j]=='_' or (frame[i][j]=='*')):
                print("{}".format(frame[i][j]).rjust(3),end=" ")
            elif(isBrick[(i,j)]!=0):
                if(game_level==3 and (i==1 or i==2)):
                    print("\033[1m"+color[3]+"{}".format(frame[i][j]).rjust(3)+"\033[0m",end=" ")
                else:
                    print("\033[1m"+color[isBrick[(i,j)].level]+"{}".format(frame[i][j]).rjust(3)+"\033[0m",end=" ")
            elif(frame[i][j]!='.'):
                print("\033[1m"+Fore.BLACK+"{}".format(frame[i][j]).rjust(3)+"\033[0m",end=" ")
            else:
                print(Fore.WHITE+"{}".format(frame[i][j]).rjust(3),end=" ")
            print(Style.RESET_ALL,end="") 
        print(end="\n")


    if(game_level==3):
       
        bar_len = 20
        filled_len = int(round(bar_len * ufo_health/ float(ufo_max_health)))

        percents = round(100.0 * ufo_health / float(ufo_max_health), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        sys.stdout.write('\n[%s] %s%s %s\r' % (bar, percents, '%', "Health"))
        print()
        sys.stdout.flush() 

        if(ufo_health==0):
            endgame()

    if count==0 and game_level!=3:
        subprocess.Popen(['aplay', './Sounds/BossDie.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
        endgame()
    
    for powerup in powerups:
        if(powerup.activated==1):
            elapsed=time.time()-powerup.start_time
            print(str(powerup.name )+ " has "+ str(15-round(elapsed))+" seconds remaining")

    if(game_level<3):
        cur_time=time.time()
        if(round(falling_brick_limit-cur_time+level_start)==0):
            falling_soon=1

        
        if (cur_time-level_start>falling_brick_limit-10 and falling_soon==0):   
            print("Time Before Bricks Start Falling: %02d seconds" %(falling_brick_limit-(cur_time-level_start)))
        
def fall_brick():
    global bricks_end
    global bricks_start
    subprocess.Popen(['aplay', './Sounds/BricksFalling.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
    for row in range(bricks_end,bricks_start-2,-1):
        for col in range(2,18):
            isBrick[(row+1,col)]=isBrick[(row,col)]
            if(frame[row][col]=='|' or frame[row][col]=='.'):
                frame[row+1][col]=frame[row][col]
            if(isBrick[(row+1,col)]!=0):
                isBrick[(row+1,col)].pos=(row+1,col)
                if(isBrick[(row+1,col)].powerup!=0):
                    isBrick[(row+1,col)].powerup.pos=(row+1,col)
                if(isBrick[(row+1,col)]).has_hit==0:
                    rainbow_bricks[:]=[tup for tup in rainbow_bricks if tup!=(row,col)]
                    rainbow_bricks.append((row+1,col))

    while(1):
        flag=0
        for i in range(2,18):
            if(isBrick[(bricks_end,i)]!=0):
                flag=1
                break
        if(flag):
            break
        bricks_end-=1

    bricks_start+=1
    bricks_end+=1

    if(bricks_end==19):
        endgame()

