from ball import Ball
import global_var as glob
from paddle import paddle
import time
from global_var import balls
import subprocess

class Powerup:

    label=''
    x=1
    y=0
    placeholder='.'
    start_time=''
    activated=0
    gravity_time=''

    def __init__(self,name,pos) -> None:
        self.name=name
        self.pos=pos
        

    def start_falling(self):
        glob.powerups.append(self)
        self.gravity_time=time.time()

    def activate(self):
        glob.powerup_check[self.name]=self
    
    def deactivate(self):
        glob.powerup_check[self.name]=0

    def update_display(self,c,newx,newy):
        # print("placeholder " + str(self.placeholder)+ " label " + str(self.label))
        glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        if(self.placeholder!='|' and self.placeholder!='-'):
            if(glob.isBrick[(self.pos)])!=0:
                glob.frame[self.pos[0]][self.pos[1]]='|'
            else:
                glob.frame[self.pos[0]][self.pos[1]]='.'
        self.placeholder=c
        glob.frame[newx][newy]=self.label

    def move(self):
        if(self.pos==(0,0)):
            return
        newx=self.pos[0]+self.x
        newx=min(newx,19)
        newx=max(newx,0)
        newy=self.pos[1]+self.y
        newy=min(newy,19)
        newy=max(newy,0)

        if(newy==0 or newy==19):
            self.y=-(self.y)
        if(glob.frame[newx][newy]=='-'):
            self.x=-(self.x)
        if (newx==19 and (newy>=paddle.start and newy<=paddle.start-1+paddle.length)):
            glob.frame[self.pos[0]][self.pos[1]]='.'
            self.pos=(0,0)    
            subprocess.Popen(['aplay', './Sounds/GetPowerup.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)          
            self.activate()
        elif(newx==19):
            glob.frame[self.pos[0]][self.pos[1]]='.'
            glob.powerups[:]=[tup for tup in glob.powerups if tup!=self]
        else:
            self.update_display(glob.frame[newx][newy],newx,newy)
            self.pos=(newx,newy)

    def check(self):
        if time.time()-self.start_time>15:
            self.deactivate()



class Double_Ball(Powerup):
    
    label='2'
    

    def __init__(self,pos) -> None:
        self.name='Ball Multiplier'
        self.pos=pos
        self.label='2'
        self.num=glob.power_num
        glob.power_num+=1
        self.affected=[]
        self.number=0
        # checkballs(self.affected)
        # print("IN INIT")

    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        temp=[]
        # checkballs(self.affected)
        for ball in balls:
            self.affected.append(ball)
        for ball in balls:
            new_ball=Ball()
            new_ball.pos=ball.pos
            new_ball.x=-(ball.x)
            new_ball.y=-(ball.y)
            new_ball.placeholder=ball.placeholder
            temp.append(new_ball)
            self.affected.append(new_ball)
        # print(temp)
        for ball in temp:
            balls.append(ball)
        # checkballs(self.affected)
        # checkballs(balls)
        
        self.number=len(self.affected)

    def deactivate(self):
        
        # checkballs(self.affected)
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        if(len(self.affected)<=self.number/2):
            glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]
        else:
            # checkballs(balls)
            for i in range (int(self.number/2),len(self.affected)):
                item=self.affected[i]
                print(item)
                balls[:]=[tup for tup in balls if tup!=item]
                glob.frame[item.pos[0]][item.pos[1]]=item.placeholder
            # checkballs(balls)

            glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]

class LongPaddle(Powerup):
    label='+'

    def __init__(self,pos) -> None:
        self.name='Enlarge Paddle'
        self.pos=pos
        self.label='+'
        self.affected=[]
    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        paddle.length+=2
        paddle.start=max(paddle.start-1,0)
        paddle.start=min(paddle.start,19-paddle.length)
        paddle.move('d')
        paddle.move('a')
    
    def deactivate(self):
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        paddle.length-=2
        paddle.start+=1
        paddle.move('d')
        paddle.move('a')
        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self] 


class ShortPaddle(Powerup):
    label='-'

    def __init__(self,pos) -> None:
        self.name='Shrink Paddle'
        self.pos=pos
        self.label='-'
        self.affected=[]
    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        paddle.length-=2
        paddle.start+=1
        paddle.move('d')
        paddle.move('a')
    
    def deactivate(self):
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        paddle.length+=2
        paddle.start=max(paddle.start-1,0)
        paddle.start=min(paddle.start,19-paddle.length)
        paddle.move('d')
        paddle.move('a')
        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self] 

class FastBall(Powerup):
    
    label='^'
    def __init__(self,pos) -> None:
        self.name='Ball Speed-Up'
        self.pos=pos
        self.affected=[]
    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        for ball in balls:
            if(ball.x>0):
                dirx=1
            else:
                dirx=-1
            ball.x+=dirx
            self.affected.append(ball)

    def deactivate(self):

        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        
        for ball in self.affected:
            if(ball.x>0):
                dirx=1
            else:
                dirx=-1
            ball.x-=dirx
        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]


class Thru_Ball(Powerup):
    
    label='!'
    

    def __init__(self,pos) -> None:
        self.name='Thru-Ball'
        self.pos=pos
        self.affected=[]

    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        for ball in balls:
            ball.go_thru+=1
            self.affected.append(ball)

    def deactivate(self):
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        for ball in self.affected:
            ball.go_thru-=1

        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]


class PaddleGrab(Powerup):
    label='H'

    def __init__(self, pos) -> None:
        self.name='Paddle Grab'
        self.pos=pos
        self.affected=[]

    def activate(self):
        glob.hold_ball+=1
        self.activated=1
        self.start_time=time.time()

    def deactivate(self):
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        glob.hold_ball-=1
        for ball in paddle.positions:
            for release in balls:
                if release.num==ball:
                    release.hold=0
        paddle.positions.clear()
   
        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]

class ShootingPaddle(Powerup):
    
    label='S'
    

    def __init__(self,pos) -> None:
        self.name='Shooting Paddle'
        self.pos=pos
        self.affected=[]

    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        paddle.shoot=1
        paddle.shoot_time=time.time()

    def deactivate(self):
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        paddle.shoot=0
        paddle.shoot_time=''
        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]


class Fireball(Powerup):
    
    label='F'
    
    def __init__(self,pos) -> None:
        self.name='Fireball'
        self.pos=pos
        self.affected=[]

    
    def activate(self):
        self.activated=1
        self.start_time=time.time()
        for ball in balls:
            ball.fireball=1
            self.affected.append(ball)

    def deactivate(self):
        if(self.pos!=(0,0)):
            glob.frame[self.pos[0]][self.pos[1]]=self.placeholder
        for ball in self.affected:
            ball.fireball=0
        self.affected.clear()
        glob.powerups[:] = [tup for tup in glob.powerups if tup!=self]