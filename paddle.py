import global_var as glob
import os
import subprocess
from brick import Brick


class Paddle:
    def __init__(self):
        self.start=7
        self.length=5
        self.hold=1
        self.positions={}
        self.shoot=0    #tells us if the shooting paddle powerup is active
        self.shoot_time=''
        
    def move(self,direction):
        subprocess.Popen(['aplay', './Sounds/PaddleSound.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
        if(direction=='d' or direction=='D'):
            self.start=min(self.start+1,19-self.length) 
        else:
            self.start=max(self.start-1,1)

        if(glob.game_level==3):
            self.move_ufo()
        
        for i in range (1,19):
            glob.frame[19][i]='.'
        for i in range(self.length):
            glob.frame[19][self.start+i]='_'
            if(self.shoot==1):
                if(i==0):
                    glob.frame[19][self.start+i]='|'
                elif(i==self.length-1):
                    glob.frame[19][self.start+i]='|'

        for key in self.positions:
            obj=self.positions[key]
            if(self.start+obj)>18:
                val=self.start+obj-18
                obj-=val
            self.positions[key]=obj
            glob.frame[19][self.start+obj]='*'

    def move_ufo(self):
        for i in range(1,3):
            for j in range (1,19):
                glob.isBrick[(i,j)]=0
        
        temp=[(2,self.start),(2,self.start+1),(2,self.start+3),(2,self.start+4),(1,self.start+1),(1,self.start+2),(1,self.start+3)]
        
        for pos in temp:
            glob.isBrick[pos]=Brick(pos)


paddle=Paddle()