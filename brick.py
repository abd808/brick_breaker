import global_var as glob
import subprocess

class Brick:

    
    score=0
    def __init__(self,pos) -> None:
        # print ("IN BRICK INIT")
        self.pos=pos
        self.powerup=0
        self.level=4
        self.boom=0
        self.has_hit=1 # tells us if it is part of rainbow blocks (0 if yes 1 if not)
    
    def give_powerup(self,powerup):
        self.powerup=powerup

    def destroy(self,ball,velocity):
        glob.score+=self.score
        glob.isBrick[self.pos]=0
        ball.placeholder='.'
        self.level=0

        if(self.powerup!=0):
            if(ball.x==1 and ball.y==0):
                self.powerup.x=2
            self.powerup.start_falling()
            self.powerup.x=velocity[0]
            self.powerup.y=velocity[1]    

        if(self.boom==1):
            subprocess.Popen(['aplay', './Sounds/Explosion.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
            glob.frame[self.pos[0]][self.pos[1]]='.'
            temp=[(self.pos[0],self.pos[1]+1),
            (self.pos[0],self.pos[1]-1),
            (self.pos[0]+1,self.pos[1]+1),
            (self.pos[0]-1,self.pos[1]+1),
            (self.pos[0]+1,self.pos[1]-1),
            (self.pos[0]-1,self.pos[1]-1),
            (self.pos[0]+1,self.pos[1]),
            (self.pos[0]-1,self.pos[1]),
            ]

            for var in temp:
                if(glob.isBrick[(var[0],var[1])]!=0 or self.level!=0):
                    glob.isBrick[(var[0],var[1])].destroy2(ball,velocity)

        self.level=0

    def destroy2(self,ball,velocity):
        glob.score+=self.score
        self.level=0
        glob.frame[self.pos[0]][self.pos[1]]='.'
        glob.isBrick[self.pos]=0
        if(self.powerup!=0):
            self.powerup.start_falling()
            self.powerup.x=velocity[0]
            self.powerup.y=velocity[1]    


        if(self.boom==1):
            subprocess.Popen(['aplay', './Sounds/Explosion.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
            temp=[(self.pos[0],self.pos[1]+1),
            (self.pos[0],self.pos[1]-1),
            (self.pos[0]+1,self.pos[1]+1),
            (self.pos[0]-1,self.pos[1]+1),
            (self.pos[0]+1,self.pos[1]-1),
            (self.pos[0]-1,self.pos[1]-1),
            (self.pos[0]+1,self.pos[1]),
            (self.pos[0]-1,self.pos[1]),
            ]

            for var in temp:
                if(glob.isBrick[(var[0],var[1])]!=0 or self.level!=0):
                    glob.isBrick[(var[0],var[1])].destroy2(ball,velocity)
                    glob.frame[var[0]][var[1]]='.'

        self.level=0
        

    def level_down(self,ball,velocity):
        subprocess.Popen(['aplay', './Sounds/BrickBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
        if(self.has_hit==0):
            self.has_hit=1
            self.score=self.level
            glob.rainbow_bricks[:]=[tup for tup in glob.rainbow_bricks if tup!=self.pos]
        if(self.level==1):
            self.destroy(ball,velocity)
        elif(self.boom==1):
            self.destroy(ball,velocity)
        elif self.level==4:
            self.level=4
            if(glob.game_level==3):
                glob.ufo_health-=1
                glob.score+=10
        elif self.level>0:
            self.level-=1

        self.is_hitting=1


class Brick1(Brick):

    score=1
    def __init__(self, pos) -> None:
        super().__init__(pos)
        self.level=1
        


class Brick2(Brick):

    score=2

    def __init__(self, pos) -> None:
        super().__init__(pos)
        self.level=2


class Brick3(Brick):

    score=3

    def __init__(self, pos) -> None:
        super().__init__(pos)
        self.level=3

class ExBrick(Brick):
     
    def __init__(self, pos) -> None:
         super().__init__(pos)
         self.level=5
         self.boom=1


    