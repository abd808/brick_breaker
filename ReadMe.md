## Description/Rules

- The game starts off with the ball attached to the paddle
- The players clicks a button to release the ball after which it enters the game space
- The ball collides with the balls and the walls on the side to change its velocity
- On bouncing on the paddle it's vertical direction gets reversed, along with a change in the horizontal velocity depending on how far from the centre of the paddle the ball hits the paddle.
- If it touches the floor of the game area , i.e not the paddle, the ball goes out of play.
- On all balls of a particular life (in case the balls have been multiplied) , going out of play, that life is lost, and all received powerups are lost.
- Each hit of a ball on a brick makes it lose a level, and on losing the max number of levels assigned to it, a brick is destroyed.
- Bricks may have powerups hidden in them which drop down when the brick is destroyed
- If the powerups are collected by the paddle, their effect lasts for 15 seconds each.
- Multiple powerups may be active at once and are stackable
- If the powerup touches the floor of the game space it is lost
- Each game has 3 lives in it   ```NOTE``` Changed to 5 in v2
- After all the blocks in a given level are cleared , you can move on to the next level, alternatively you can click on the L button to skip to the next level.

## Types of Bricks

- Levels   
    - 1
    - 2
    - 3
    - Each of these levels implies the number of hits required to destroy the respective bricks.
    - On each hit the brick drops a level
    - Each level has a color associated with it
- Unbreakable: As the name suggests, they do not get broken unless the Thru-Ball powerup is in effect
- Exploding 
    - Found in groups of 6, each one explodes on a single hit
    - It also destroys its direct neighbours, i.e horizontal , vertical and diagonal

## Powerups

***Powerups now get released with the velocity which the ball has before colliding with the brick, and are accelerated downwards at a constant rate***

- Ball Multiplier : `2`
    - Each current ball on screen is split into two and the new ball goes in the opposite direction as the new one
    - On the end of the time slice, it destroys all the new balls it had created

- Expand Paddle : `+`
    - Increases paddle length by 2 units for 15 seconds and then reverses its effects

- Shrink Paddle : `-`
    - Does the opposite of Expand Paddle

- Speed Up : `^`
    - Increases velocity of ball for 15 seconds

- Thru-Ball : `!`
    - Allows the ball through and destroy any brick, including unbreakable bricks , for 15 seconds

- Paddle Grab : `H`
    - Everytime the ball touches the paddle the changes in velocity are made like they would in a normal collision, but the ball gets stuck on the paddle, and is moved along with the paddle, and can be released by pressing `H` , and the ball moves as it would have after a normal collision

- FireBall : `F`
    - Converts any brick it touches into an exploding brick

- Shooting Paddle : `S`
    - On powerup being obtained, paddle shoots bullets continuously from its two ends , each bullet has the same strength as a ball, but only has capacity of 1 collision per bullet, hence dissappears after its first collision , either with a brick or the roof/ceiling


## Running the Game

- Run python3 `main.py` in its directory 


## Game Tutorial

    ## All the keys are case insensitive

- Press `S` to start the game 
- Press `H` key to release the ball whenever a new life is started to release it from the paddle hold
- Press `E` key to exit the game at any point mid-game
- Use `D` to move paddle right
- Use `A` to move paddle left
- Use `L` to skip up levels
- The farther away from the centre of the paddle the ball hits, the more the deviation in that direction of the horizontal velocity



## Additional Features

- Sound has been added to most of the game actions
- ### Rainbow Bricks
    - The color and hardness of these bricks keeps changing until touched by a laser or a ball, and then the levels move downwards from there
- ### Boss Level
    - Consists of a UFO and a few unbreakable bricks
    - UFO moves along with the paddle
    - Periodically releases bombs downwards which on hitting the paddle cost a life
    - It has two layers of bricks that it casts ass defence , once at 75% of its maximum health, and one at 50%.
    - Each hit by the ball costs it one health point