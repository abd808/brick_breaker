import global_var as glob
import sys
import math
from input import Get,input_to
from paddle import paddle
from global_var import balls,laser_balls
import subprocess

class Ball:

    def __init__(self) -> None:
        self.x=-1
        self.hold=0
        self.hold_position=-1
        self.y=0
        self.pos=(18,10)
        self.placeholder='_'
        self.go_thru=0
        self.num=glob.num
        glob.num+=1
        self.symbol='*'
        self.laser=0 # tells us whether this particular ball is a laser
        self.ufo=0
        self.fireball=0

    def ball_die(self):
        for obj in glob.powerups:
            obj.deactivate()
        glob.printframe()
        if(glob.lives==0):
            print("Game Over, All lives Lost")
            print("Current Score: "+ str(glob.score))
            sys.exit()
        print("LIFE LOST, Press C to Continue")
        
        while(1):
            text=input_to(Get())
            if(text=='C' or text=='c'):
                break

    def reset(self):
        self.fireball=0
        self.go_thru=0
        self.x=-1
        self.y=0
        self.pos=(18,10)
        self.placeholder='_'
        self.hold=1
        paddle.positions[self.num]=math.floor(paddle.length/2)
        paddle.move('d')
        paddle.move('a')

    def update_display(self,c,newx,newy):
        glob.frame[int(self.pos[0])][int(self.pos[1])]=self.placeholder
        self.placeholder=c
        glob.frame[int(newx)][int(newy)]=self.symbol

    def inc_x(self,val):
        self.x+=val
    
    def inc_y(self,val):
        self.y+=val

    def collide_horizontal(self):
        self.y=-(self.y)

    def collide_vertical(self):
        self.x=-(self.x)

    def move(self):
        self.x=int(self.x)
        self.y=int(self.y)
        if(self.x>0):
            dirx=1
        else:
            dirx=-1
        if(self.y>0):
            diry=1
        else:
            diry=-1

        done=0
        absx=abs(self.x)
        absy=abs(self.y)
        if(absx>absy):
            for i in range (1,absx+1):
                if (absy!=0):
                    newy=self.pos[1]+diry*round(i/absy)
                else:
                    newy=self.pos[1]
                newx=self.pos[0]+i*dirx
                newx=max(0,newx)
                newx=min(newx,19)
                newy=max(0,newy)
                newy=min(19,newy)
                # print("First If")
                # print(newx)
                # print(newy)

                if glob.frame[int(newx)][int(newy)]=='-':
                    self.collide_vertical()
                    self.update_display('-',newx,newy)
                    self.pos=(newx,newy)
                    done=1
                    subprocess.Popen(['aplay', './Sounds/WallRoofBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
                    if(self.laser==1):
                        glob.frame[int(self.pos[0])][int(self.pos[1])]=self.placeholder
                        laser_balls[:] = [tup for tup in laser_balls if tup!=self]
                        for obj in glob.powerups:
                            obj.affected[:]=[tup for tup in obj.affected if tup!=self]
                    break
                elif glob.isBrick[(newx,newy)]!=0:
                    if(self.fireball):
                        glob.isBrick[(newx,newy)].boom=1
                    if(self.ufo):
                        self.update_display('|',newx,newy)
                        self.pos=(newx,newy)
                        return
                    temp=(self.x,self.y)
                    if(self.go_thru>0):
                        glob.isBrick[(newx,newy)].destroy(self,temp)
                    elif(absy==0):
                        self.collide_vertical()
                    elif(glob.isBrick[(newx+dirx,newy-diry)]==0):
                        self.collide_horizontal()
                    else:
                        self.collide_vertical()
                    self.update_display('|',newx,newy)
                    if glob.isBrick[(newx,newy)]!=0:
                        glob.isBrick[(newx,newy)].level_down(self,temp)
                    self.pos=(newx,newy)
                    if(self.laser==1):
                        glob.frame[int(self.pos[0])][int(self.pos[1])]=self.placeholder
                        laser_balls[:] = [tup for tup in laser_balls if tup!=self]
                        for obj in glob.powerups:
                            obj.affected[:]=[tup for tup in obj.affected if tup!=self]
                    done=1
                    break
                elif (newy==0 or newy==19):
                    self.collide_horizontal()
                    self.update_display('|',newx,newy)
                    self.pos=(newx,newy)
                    subprocess.Popen(['aplay', './Sounds/WallRoofBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
                    done=1
                    break
                elif(newx==19 and (newy>=paddle.start and newy<=paddle.start-1+paddle.length)):
                    if(self.ufo):
                        self.update_display('.',newx,newy)
                        for ball in laser_balls:
                            glob.frame[ball.pos[0]][ball.pos[1]]=ball.placeholder
                        laser_balls.clear()
                        glob.frame[balls[0].pos[0]][balls[0].pos[1]]=balls[0].placeholder
                        balls[0].ball_die()
                        balls[0].reset()
                        glob.lives-=1
                        if(glob.lives==0):
                            print("Game Over, All lives Lost")
                            print("Current Score: "+ str(glob.score))
                            sys.exit()
                        return
                    diff=newy-paddle.start
                    mid=math.floor(paddle.length/2)
                    self.y+=(diff-mid)
                    self.collide_vertical()
                    self.update_display('_',newx,newy)
                    subprocess.Popen(['aplay', './Sounds/WallRoofBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
                    self.pos=(newx,newy)
                    if glob.hold_ball>0:
                        self.hold=1
                        paddle.positions[self.num]=newy-paddle.start
                    done=1
                    if(glob.falling_soon==1):
                        glob.fall_brick()
                    break
                elif(newx==19):
                    if(self.ufo):
                        self.update_display('.',newx,newy)
                        laser_balls[:]=[tup for tup in laser_balls if tup!=self]
                        return
                    if(len(balls)>1):
                        glob.frame[int(self.pos[0])][int(self.pos[1])]='.'
                        balls[:] = [tup for tup in balls if tup!=self]
                        for obj in glob.powerups:
                            obj.affected[:]=[tup for tup in obj.affected if tup!=self]
                        done=1
                        break
                    for ball in laser_balls:
                        glob.frame[ball.pos[0]][ball.pos[1]]=ball.placeholder
                    for ball in glob.powerups:
                        glob.frame[ball.pos[0]][ball.pos[1]]=ball.placeholder
                        ball.deactivate()
                    glob.powerups.clear()
                    laser_balls.clear()
                    glob.lives-=1                    
                    self.update_display('.',newx,newy)
                    self.ball_die()
                    self.reset()
                    done=1  
                    break        

        else:
            for i in range (1,absy+1):
                if(absx!=0):
                    newx=self.pos[0]+dirx*round(i/absx)
                else:
                    newx=self.pos[0]
                    
                newy=self.pos[1]+i*diry
                newx=max(0,newx)
                newx=min(newx,19)
                newy=max(0,newy)
                newy=min(19,newy)
                # print(newx)
                # print(newy)
                # print(glob.isBrick[(newx,newy)])
                if glob.frame[int(newx)][int(newy)]=='-':
                    # print("HI")
                    self.collide_vertical()
                    self.update_display('-',newx,newy)
                    self.pos=(newx,newy)
                    done=1
                    subprocess.Popen(['aplay', './Sounds/WallRoofBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
                    if(self.laser==1):
                        glob.frame[int(self.pos[0])][int(self.pos[1])]=self.placeholder
                        laser_balls[:] = [tup for tup in laser_balls if tup!=self]
                        for obj in glob.powerups:
                            obj.affected[:]=[tup for tup in obj.affected if tup!=self]
                    break
                elif glob.isBrick[(newx,newy)]!=0:
                    if(self.fireball):
                        glob.isBrick[(newx,newy)].boom=1
                    if(self.ufo):
                        self.update_display('|',newx,newy)
                        self.pos=(newx,newy)
                        return
                    temp=(self.x,self.y)
                    if(self.go_thru>0):
                        glob.isBrick[(newx,newy)].destroy(self,temp)
                    elif(glob.isBrick[(newx-dirx,newy+diry)]==0):
                        self.collide_vertical()
                    else:
                        self.collide_horizontal()
                    self.update_display('|',newx,newy)
                    if glob.isBrick[(newx,newy)]!=0:
                        glob.isBrick[(newx,newy)].level_down(self,temp)
                    self.pos=(newx,newy)
                    if(self.laser==1):
                        glob.frame[int(self.pos[0])][int(self.pos[1])]=self.placeholder
                        laser_balls[:] = [tup for tup in laser_balls if tup!=self]
                        for obj in glob.powerups:
                            obj.affected[:]=[tup for tup in obj.affected if tup!=self]
                    done=1
                    break
                elif (newy==0 or newy==19):
                    self.collide_horizontal()
                    self.update_display('|',newx,newy)
                    self.pos=(newx,newy)
                    subprocess.Popen(['aplay', './Sounds/WallRoofBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
                    done=1
                    break
                elif(newx==19 and (newy>=paddle.start and newy<=paddle.start-1+paddle.length)):
                    if(self.ufo):
                        self.update_display('.',newx,newy)
                        for ball in laser_balls:
                            glob.frame[ball.pos[0]][ball.pos[1]]=ball.placeholder
                        laser_balls.clear()
                        glob.frame[balls[0].pos[0]][balls[0].pos[1]]=balls[0].placeholder
                        balls[0].ball_die()
                        balls[0].reset()
                        glob.lives-=1
                        if(glob.lives==0):
                            print("Game Over, All lives Lost")
                            print("Current Score: "+ str(glob.score))
                            sys.exit()
                        return
                    diff=newy-paddle.start
                    mid=math.floor(paddle.length/2)
                    self.collide_vertical()
                    self.y+=(diff-mid)
                    self.update_display('_',newx,newy)
                    self.pos=(newx,newy)
                    subprocess.Popen(['aplay', './Sounds/WallRoofBounce.wav'],stdout=subprocess.DEVNULL,stderr=subprocess.STDOUT)
                    if glob.hold_ball>0:
                        self.hold=1
                        paddle.positions[self.num]=newy-paddle.start
                    done=1
                    if(glob.falling_soon==1):
                        glob.fall_brick()
                    break
                elif(newx==19):
                    if(self.ufo):
                        self.update_display('.',newx,newy)
                        laser_balls[:]=[tup for tup in laser_balls if tup!=self]
                        return
                    if(len(balls)>1):
                        glob.frame[int(self.pos[0])][int(self.pos[1])]='.'
                        balls[:] = [tup for tup in balls if tup!=self]
                        for obj in glob.powerups:
                            obj.affected[:]=[tup for tup in obj.affected if tup!=self]
                        done=1
                        break
                    for ball in laser_balls:
                        glob.frame[ball.pos[0]][ball.pos[1]]=ball.placeholder
                    for ball in glob.powerups:
                        glob.frame[ball.pos[0]][ball.pos[1]]=ball.placeholder
                        ball.deactivate()
                    glob.powerups.clear()
                    laser_balls.clear()
                    glob.lives-=1
                    self.update_display('.',newx,newy)
                    self.ball_die()
                    self.reset()
                    done=1
                    break
        if(done==0):
            print(str(self.num) + str(self.pos))
            glob.frame[int(self.pos[0])][int(self.pos[1])]=self.placeholder
            self.pos=(int(self.pos[0]+self.x),int(self.pos[1]+self.y))
            self.placeholder='.'     
            print(str(self.num) + str(self.pos))       
            glob.frame[self.pos[0]][self.pos[1]]=self.symbol
            # print(self.pos)
            # sys.exit()


